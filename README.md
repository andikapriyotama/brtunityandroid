Source game **BRT Go!** untuk keperluan dokumentasi.

Projek dibuat dengan versi **5.3.4p4** dan perlu terinstall **Android Build Support**. Membuka projek dengan Unity versi lebih baru perlu reimport, yang akibatnya hasil yang mungkin tidak bisa ditebak.

---

## Credit:

- **Game Artist**: Andika Priyotama D.
- **Game Programmer**: Andika Priyotama D.
