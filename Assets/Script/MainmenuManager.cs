﻿using UnityEngine;
using System.Collections;

using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MainmenuManager : MonoBehaviour
{
    public string sceneTombolPlay = "LevelUIResult";
    public string sceneTombolHow = "Howto";

    public GameObject panelKreator;
    public GameObject howtoScreen;
    public bool panelKreatorAktif = false;

    void Start()
    {
        panelKreator.SetActive(panelKreatorAktif);
    }

	public void menujuGame()
    {
        if (!panelKreatorAktif)
        {
            SoundManager.instance.bunyiTombol();

            StartCoroutine(DelaySceneLoad());
        }
    }

    public void menujuHow()
    {
        if (!panelKreatorAktif)
        {
            SoundManager.instance.bunyiTombol();

            howtoScreen.SetActive(true);
        }
    }

    public void kreatorTampilkan()
    {
        if (!panelKreatorAktif)
        {
            SoundManager.instance.bunyiTombol();

            panelKreator.SetActive(true);
        }
    }

    public void kreatorClose()
    {
        SoundManager.instance.bunyiTombol();

        panelKreator.SetActive(false);
    }

    public void aplikasiQuit()
    {
        Application.Quit();
    }

    IEnumerator DelaySceneLoad()
    {
        yield return new WaitForSeconds(.38f);
        SceneManager.LoadScene(sceneTombolPlay);
    }
}