﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GeneratorKendaraan : MonoBehaviour
{
    public GameObject[] prefabKendaraan;

    [Header("Timing")]
    public float delayAwal = 0f;
    public float interval = 4.5f;

    [Header("Transform")]
    public float batasAtas = 600f;
    public Vector2 speedKendaraan = new Vector2(-1.5f, 0);

    [Header("Counter")]
    public int urutanKendaraan;

    [HideInInspector]
    public List<GameObject> listKendaraan = new List<GameObject>();

    private LevelManager levelManager;

    void Start ()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();

        InvokeRepeating("munculkanKendaraan", delayAwal, interval);
    }

    void munculkanKendaraan()
    {
        if (!levelManager.selesai)
        {
            // Acak jenis kendaraan
            int i = Random.Range(0, prefabKendaraan.Length);

            GameObject kendaraan = prefabKendaraan[i];

            // Posisi clone ditentukan dari
            // (1) posisi titik di ujung kanan layar
            // (2) ditambah jarak dari titik luar ke titik tengah sprite

            // (1) x:
            Vector3 layar = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
            Sprite sp = kendaraan.GetComponent<SpriteRenderer>().sprite;

            //// (2) y:
            //float yScreenRandom = Random.Range(
            //    0f,
            //    batasAtas
            //);
            //float yScreenRasio = (yScreenRandom / 600f) * Screen.height;

            //Vector3 titikWorld = Camera.main.ScreenToWorldPoint(new Vector3(0, yScreenRasio, 0));

            //// posisi:
            //float posX = layar.x + (sp.bounds.size.x / 2);
            //float posY = titikWorld.y;

            //if (urutanKendaraan < 1)
            //    posY = -1.1f;

            //// cek posisi batas bawah
            //if (posY < (-6f + sp.bounds.max.y * 2))
            //{
            //    //Debug.Log("Kengisoren!");
            //    posY = -6f + sp.bounds.max.y * 2;
            //}
            //// cek posisi batas atas
            //else if (posY > (0f + sp.bounds.min.y))
            //{
            //    //Debug.Log("Kedhuwuren!");
            //    posY = 0f + sp.bounds.min.y;
            //}

            float posX = layar.x + (sp.bounds.size.x / 2);
            float posY = Random.Range(-0.85f, -4.1f);

            if (urutanKendaraan < 1)
                posY = -1.1f;

            if (posY > -1 * 5.0f / 3.0f)
                kendaraan.GetComponent<SpriteRenderer>().sortingOrder = 1;
            else if (posY > -1 * 5.0f * 2.0f / 3.0f)
                kendaraan.GetComponent<SpriteRenderer>().sortingOrder = 3;
            else if (posY > -5)
                kendaraan.GetComponent<SpriteRenderer>().sortingOrder = 5;

            //Debug.Log("Kendaraan: " + kendaraan.GetComponent<SpriteRenderer>().sortingOrder);

            Vector2 posisi = new Vector2(
                posX,
                posY
            );

            //Instantiate(kendaraan, posisi, transform.rotation);

            listKendaraan.Add(Instantiate(kendaraan, posisi, transform.rotation) as GameObject);

            if (urutanKendaraan < 2)

                urutanKendaraan++;
        }
    }
}