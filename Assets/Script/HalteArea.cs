﻿using UnityEngine;
using System.Collections;

public class HalteArea : MonoBehaviour
{
    private LevelManager levelManager;

    void Start ()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
	}
	
	void OnTriggerEnter2D (Collider2D objekKena)
    {
	    if (objekKena.name == "Player")
        {
            levelManager.modeAngkut = true;
        }
	}

    void OnTriggerExit2D(Collider2D objekKena)
    {
        if (objekKena.name == "Player")
        {
            levelManager.modeAngkut = false;
        }
    }
}