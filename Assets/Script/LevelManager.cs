﻿using UnityEngine;
using System.Collections;

using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance = null;

    public string sceneLevelSekarang = "Level1";
    public string sceneLevelSelanjutnya = "Level2";
    public string sceneMenuUtama = "MenuUtama";

    [Header("Setting Level")]
    public float jarakMax = 60;
    public float pelajarMax = 20;

    [Header("Setting Display Skor")]
    public Text teksPelajar;
    public Text teksUmum;
    public Text teksKoin;

    [Header("Setting Panel Misi")]
    public GameObject panelMisiOverlay;
    public GameObject panelMenang;
    public GameObject panelKalah;

    [Header("Setting Panel Hasil")]
    public GameObject panelHasil;
    public Button tombolNext;
    public Button tombolRestart1;
    public Button tombolRestart2;
    public bool panelHasilTesVisible = false;
    public Text hasilPelajar;
    public Text hasilUmum;
    public Text hasilKoin;

    [HideInInspector]
    public long angkutPenumpang;
    [HideInInspector]
    public long jumlahPelajar;
    [HideInInspector]
    public long jumlahUmum;
    [HideInInspector] 
    public long jumlahKoin;
    [HideInInspector]
    public bool selesai = false;
    [HideInInspector]
    public bool modeAngkut;

    void Awake()
    {
        instance = this;
    }
    
    void Start ()
    {
        //State level/permainan sedang berjalan (belum selesai)
        selesai = false;
        
        //Set display skor awal
        teksPelajar.text = "0/" + pelajarMax;
        teksUmum.text = "0";
        teksKoin.text = "0";

        //Panel Hasil disembunyikan di awal
        panelHasil.SetActive(panelHasilTesVisible);

        modeAngkut = false;     //baiknya bukan di sini,
                                //tapi entah di player atau penumpangnya
                                //karena mode ini untuk state gameplay-nya, bukan state levelnya
	}

    public void updatePelajar()
    {
        jumlahPelajar++;

        teksPelajar.text = jumlahPelajar + "/" + pelajarMax;
    }

    public void updateUmum()
    {
        jumlahUmum++;
        teksUmum.text = jumlahUmum.ToString();

        jumlahKoin++;
        teksKoin.text = jumlahKoin.ToString();
    }

    public void updateKoin()
    {
        jumlahKoin++;

        teksKoin.text = jumlahKoin.ToString();
    }

    //Trigger, Ketika game selesai:
    //1. Waktu/jarak habis (KalkulasiBar)
    //2. Penumpang pelajar udah target (PenumpangController)
    //3. Player benturan dengan kendaraan (PlayerController > Collision)
    public void cekGameover()
    {
        selesai = true;

        if (!panelMisiOverlay.activeSelf)
        {
            //Menang
            if (jumlahPelajar == pelajarMax)
            {
                SoundManager.instance.bunyiMenang();

                //panelMisiOverlay.SetActive(true);
                Invoke("tampilkanPanelMisi", .6f);

                tombolNext.gameObject.SetActive(true);
                tombolRestart1.gameObject.SetActive(false);
                tombolRestart2.gameObject.SetActive(true);
                panelMenang.SetActive(true);
                panelKalah.SetActive(false);

                Invoke("tampilkanPanelHasil", 2f);
            }
            //Kalah
            else
            {
                tombolNext.interactable = false;

                //panelMisiOverlay.SetActive(true);
                Invoke("tampilkanPanelMisi", .6f);

                tombolNext.gameObject.SetActive(false);
                tombolRestart1.gameObject.SetActive(true);
                tombolRestart2.gameObject.SetActive(false);
                panelKalah.SetActive(true);
                panelMenang.SetActive(false);

                Invoke("tampilkanPanelHasil", 2f);
            }
        }
    }
    
    public void tampilkanPanelMisi()
    {
        if (jumlahPelajar == pelajarMax)
            SoundManager.instance.bunyiMusikMenang();
        else
            SoundManager.instance.bunyiKalah();

        panelMisiOverlay.SetActive(true);
    }

    public void tampilkanPanelHasil()
    {
        hasilPelajar.text = jumlahPelajar.ToString();

        panelHasil.SetActive(true);

        panelMisiOverlay.SetActive(false);
    }
    
    public void restartGame()
    {
        //selesai = false;

        SoundManager.instance.bunyiTombol();

        StartCoroutine(DelaySceneLoad(SceneManager.GetActiveScene().name));
    }

    public void keluarGame()
    {
        //selesai = false;

        SoundManager.instance.bunyiTombol();

        StartCoroutine(DelaySceneLoad(sceneMenuUtama));
    }

    public void naikLevel()
    {
        //selesai = false;

        SoundManager.instance.bunyiTombol();

        StartCoroutine(DelaySceneLoad(sceneLevelSelanjutnya));
    }

    IEnumerator DelaySceneLoad(string sceneTujuan)
    {
        yield return new WaitForSeconds(.38f);

        SceneManager.LoadScene(sceneTujuan);
    }
}