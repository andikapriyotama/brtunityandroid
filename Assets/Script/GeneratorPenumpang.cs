﻿using UnityEngine;
using System.Collections;

public class GeneratorPenumpang : MonoBehaviour
{
    public int kapasitasPenumpang = 3;
    public GameObject[] prefabPenumpang;

	void Start ()
    {
        float xPenumpang = -0.8f;
        for (int i = 0; i < kapasitasPenumpang; i++)
        {
            int idx = Random.Range(0, prefabPenumpang.Length);
            GameObject penumpang = Instantiate(
                prefabPenumpang[idx],
                new Vector2(xPenumpang, 1),
                transform.rotation
            ) as GameObject;

            penumpang.transform.parent = gameObject.transform;
            penumpang.transform.localPosition = new Vector2(xPenumpang, .1f);

            xPenumpang += 0.5f;
        }
	}
}