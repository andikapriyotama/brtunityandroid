﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class HowtomenuManager : MonoBehaviour
{
    public int displayAktif = 1; 
    
    public GameObject how1;
    public GameObject how2;
    public GameObject how3;
    public GameObject tombolPrev;
    public GameObject tombolNext;
    public GameObject tombolClose;

	void Start ()
    {
        displayAktif = 1;

        how2.SetActive(true);
        how2.SetActive(false);
        how3.SetActive(false);

        tombolPrev.SetActive(false);
        tombolNext.SetActive(true);
        tombolClose.SetActive(false);
	}

    public void aksiPrev()
    {
        SoundManager.instance.bunyiTombol();

        displayAktif--;

        tampilkanDisplay(displayAktif);
    }

    public void aksiNext()
    {
        SoundManager.instance.bunyiTombol();

        displayAktif++;

        tampilkanDisplay(displayAktif);
    }

    public void aksiClose()
    {
        SoundManager.instance.bunyiTombol();

        displayAktif = 1;

        StartCoroutine(DelaySceneLoad("MenuUtama"));
        //SceneManager.LoadScene("MenuUtama");
    }

    private void tampilkanDisplay(int displayAktif)
    {
        switch (displayAktif)
        {
            case 1:
                how1.SetActive(true);
                how2.SetActive(false);
                how3.SetActive(false);
                tombolPrev.SetActive(false);
                tombolNext.SetActive(true);
                tombolClose.SetActive(false);
                break;

            case 2:
                how1.SetActive(false);
                how2.SetActive(true);
                how3.SetActive(false);
                tombolPrev.SetActive(true);
                tombolNext.SetActive(true);
                tombolClose.SetActive(false);
                break;

            case 3:
                how1.SetActive(false);
                how2.SetActive(false);
                how3.SetActive(true);
                tombolPrev.SetActive(true);
                tombolNext.SetActive(false);
                tombolClose.SetActive(true);
                break;
        }
    }

    IEnumerator DelaySceneLoad(string sceneTujuan)
    {
        yield return new WaitForSeconds(.38f);
        SceneManager.LoadScene(sceneTujuan);
    }
}
