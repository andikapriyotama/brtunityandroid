﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SkripNilaiPenumpang : MonoBehaviour
{
    public LevelManager levelManager;

    private Text uiText;
    private long jumlahPenumpang = 0;

    void Start()
    {
        // LevelManager
        //levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();

        // NilaiPenumpang
        uiText = GetComponent<Text>();

        updateScoreCounter();
    }

    public void tambahPenumpang()
    {
        jumlahPenumpang++;
        levelManager.angkutPenumpang = jumlahPenumpang;

        updateScoreCounter();
    }

    private void updateScoreCounter()
    {
        levelManager.jumlahPelajar = jumlahPenumpang;

        uiText.text = jumlahPenumpang.ToString();
    }
}