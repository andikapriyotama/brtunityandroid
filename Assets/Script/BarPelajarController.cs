﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BarPelajarController : MonoBehaviour
{
    public LevelManager levelManager;

    public float pelajarMax = 20f; // satuan detik
    public float lebarAwal = 26f;
    public float intervalJarak = 10f;

    private Image img;

    private RectTransform rect;
    private Vector2 rectAsli;
    private float tambah;
    private float lebarIncr;
    private float lebarJalan;

    void Start()
    {
        pelajarMax = levelManager.pelajarMax;

        img = GetComponent<Image>();
        img.fillAmount = 0;

        /*rect = GetComponent<RectTransform>();
        rectAsli = rect.sizeDelta;

        rect.sizeDelta = new Vector2(lebarAwal, rectAsli.y);*/

        tambah = 1 / pelajarMax;
        lebarIncr = tambah / 10;
    }

    void Update()
    {
        //lebar = rectAsli.x * (levelManager.jumlahPelajar / pelajarMax);
        //img.fillAmount = levelManager.jumlahPelajar / pelajarMax;

        tambah = levelManager.jumlahPelajar / pelajarMax;

        if (lebarJalan < tambah)
        {
            lebarJalan += lebarIncr;

            if (lebarJalan > tambah)

                lebarJalan = tambah;

            if (lebarJalan > 1)

                lebarJalan = 1;

            img.fillAmount = lebarJalan;
        }

        /*if (lebarJalan < tambah)
        {
            lebarJalan += lebarIncr;

            if (lebarJalan > tambah)

                lebarJalan = tambah;

            if (lebarJalan > rectAsli.x)

                lebarJalan = rectAsli.x;

            rect.sizeDelta = new Vector2(lebarJalan, rectAsli.y);
        }*/
    }
}