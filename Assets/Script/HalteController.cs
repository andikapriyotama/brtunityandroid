﻿using UnityEngine;
using System.Collections;

public class HalteController : MonoBehaviour
{
    public float speedX = 2f;
    public float arah = -1;

    private ScrollingController jalanScroll;

    void Start()
    {
        jalanScroll = GameObject.Find("lampuhidran1").GetComponent<ScrollingController>();

        speedX = jalanScroll.speedX;
    }

    void Update()
    {
        Vector2 gerak = new Vector2(speedX * arah, 0);

        gerak *= Time.deltaTime;
        transform.Translate(gerak);
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}