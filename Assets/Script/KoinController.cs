﻿using UnityEngine;
using System.Collections;

using UnityEngine.Audio;

public class KoinController : MonoBehaviour
{
    public float speedX = 0.5f;
    public float arah = -1;

    private LevelManager levelManager;
    private Vector2 gerak;
    private ScrollingController jalanScroll;

    void Start()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();

        jalanScroll = GameObject.Find("lampuhidran1").GetComponent<ScrollingController>();

        speedX = jalanScroll.speedX;
    }

    void Update()
    {
        gerak = new Vector2(speedX * arah, 0);

        gerak *= Time.deltaTime;
        transform.Translate(gerak);
    }

    void OnTriggerEnter2D(Collider2D objekKena)
    {
        if (objekKena.name == "Player" && !levelManager.selesai)
        {
            SoundManager.instance.bunyiKoin();
            levelManager.updateKoin();
            Destroy(gameObject);
        }
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}