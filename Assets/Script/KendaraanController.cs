﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KendaraanController : MonoBehaviour
{
	public Vector2 speed = new Vector2(-2,0);

    private Rigidbody2D rbKendaraan;
    private GeneratorKendaraan generator;

    void Start()
    {
        generator = GameObject.Find("GeneratorKendaraan").GetComponent<GeneratorKendaraan>();
        rbKendaraan = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        rbKendaraan.velocity = generator.speedKendaraan;
    }

    void OnBecameInvisible()
    {
        if (generator.listKendaraan.Count > 0)
        {
            //Debug.Log("index: "+generator.listKendaraan.IndexOf(gameObject));
            generator.listKendaraan.RemoveAt(generator.listKendaraan.IndexOf(gameObject));
            Destroy(gameObject);
            //Debug.Log(generator.listKendaraan.Count);
        }
    } 
}