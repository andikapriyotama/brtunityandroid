﻿using UnityEngine;
using System.Collections;

public class GeneratorKoin : MonoBehaviour
{
    [Header("Referensi")]
    public GameObject prefabKoin;
    public GeneratorHalte generatorHalte;
    public GeneratorKendaraan generatorKendaraan;

    [Header("Timing")]
    public float delayAwal = 0f;
    public float interval = 3;

    [Header("Transform")]
    public int banyaknya = 10;
    public float batasAtas = 200f;

    private LevelManager levelManager;

    void Start ()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();

        InvokeRepeating("munculkanKoin", delayAwal, interval);
    }

    void munculkanKoin()
    {
        // hanya ketika game masih berjalan/belum "selesai"
        if(!levelManager.selesai && generatorKendaraan.urutanKendaraan >= 1)
        {
            // Posisi clone ditentukan dari
            // (1) posisi titik di ujung kanan layar
            // (2) ditambah jarak dari titik luar ke titik tengah sprite

            // (1) x:
            Vector3 layar = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
            Sprite sp = prefabKoin.GetComponent<SpriteRenderer>().sprite;

            // (2) y:
            float yScreenRandom = Random.Range(
                0f,
                batasAtas
            );
            float yScreenRasio = (yScreenRandom / 600f) * Screen.height;

            Vector3 titikWorld = Camera.main.ScreenToWorldPoint(new Vector3(0, yScreenRasio, 0));

            // posisi:
            float posX = layar.x + (sp.bounds.size.x / 2);
            float posY = titikWorld.y;

            // cek posisi batas bawah
            if (posY < (-6f + sp.bounds.max.y*2))
            {
                //Debug.Log("Kengisoren!");
                posY = -6f + sp.bounds.max.y*2;
            }
            // cek posisi batas atas
            else if (posY > (0f + sp.bounds.min.y))
            {
                //Debug.Log("Kedhuwuren!");
                posY = 0f + sp.bounds.min.y;
            }

            if (posY > -1 * 5.0f / 3.0f)
            {
                posY = -5 * 1 / 4;
                prefabKoin.GetComponent<SpriteRenderer>().sortingOrder = 1;
            }
            else if (posY > -1 * 5.0f * 2.0f / 3.0f)
            {
                posY = -5 * 2 / 4;
                prefabKoin.GetComponent<SpriteRenderer>().sortingOrder = 3;
            }
            else if (posY > -5)
            {
                posY = -5 * 3 / 4;
                prefabKoin.GetComponent<SpriteRenderer>().sortingOrder = 5;
            }

            for (int i = 0; i < banyaknya; i++)
            {
                Vector2 posisi = new Vector2(posX, posY);

                Instantiate(prefabKoin, posisi, transform.rotation);

                posX += sp.bounds.size.x + 0.05f;
                
                //Debug.Log("Koin: " + koin.GetComponent<SpriteRenderer>().sortingOrder);
            }
        }
    }
}