﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GeneratorHalte : MonoBehaviour
{
    [Header("Referensi")]
    public GameObject prefabHalte;
    public GeneratorKendaraan generatorKendaraan;

    [Header("Timing")]
    public float delayAwal = 0f;
    public float interval = 5f;

    [Header("Counter")]
    public int urutanHalte;

    private LevelManager levelManager;

    void Start()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();

        InvokeRepeating("munculkanHalte", delayAwal, interval);
    }

    void munculkanHalte()
    {
        //Buat Halte hanya ketika game masih jalan
        //if (!levelManager.selesai && generatorKendaraan.urutanKendaraan >= 0)
        if (!levelManager.selesai)
        {
            Vector3 layar = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

            Sprite spriteHalte = prefabHalte.GetComponent<SpriteRenderer>().sprite;

            float posX = layar.x + (spriteHalte.bounds.size.x / 2);
            float posY = 0.91f;

            Vector2 posisi = new Vector2(posX, posY);

            Instantiate(prefabHalte, posisi, transform.rotation);

            //Hitung yang sudah dibuat
            if (urutanHalte < 1)

                urutanHalte++;
        }
    }
}