﻿using UnityEngine;
using System.Collections;

public class JalananController : MonoBehaviour
{
    public float speed = 0.5f;
    public float arah = -1;

    void Update()
    {
        Vector2 gerak = new Vector2(speed * arah, 0);
        gerak *= Time.deltaTime;
        transform.Translate(gerak);

        float centerInvisible = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).x - transform.position.x;

        if (centerInvisible >= 8.53f)
        {
            float posisiXViewport = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0)).x;
            float posisiPindah = posisiXViewport + 8.53f;

            transform.position = new Vector2(posisiPindah, transform.position.y);
        }
    }
}