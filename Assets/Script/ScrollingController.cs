﻿using UnityEngine;
using System.Collections;

public class ScrollingController : MonoBehaviour
{
    public float speedX = 2f;
    public float arah = -1;
    public bool linkKeKamera = false;
    public bool ngeloop = false;

    void Update()
    {
        Vector2 gerak = new Vector2(speedX * arah, 0);

        gerak *= Time.deltaTime;

        transform.Translate(gerak);

        if (linkKeKamera)
        {
            Camera.main.transform.Translate(gerak);
        }

        if (ngeloop)
        {
            float selisihCenter = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).x - transform.position.x;

            if (selisihCenter >= (21.8f - 8.53f))
            {
                float posisiXViewport = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0)).x;
                float posisiPindah = posisiXViewport + (29.6f - 8.53f);

                transform.position = new Vector2(posisiPindah, transform.position.y);
            }
        }
    }
}
