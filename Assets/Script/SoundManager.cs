﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    [Header("AudioSource")]
    public AudioSource musik;
    public AudioSource soundTombol;
    public AudioSource soundKoin;
    public AudioSource soundAngkut;
    public AudioSource soundBel;
    public AudioSource soundBenturan;
    public AudioSource soundCrash;
    public AudioSource soundMenang;
    public AudioSource soundMenang1;
    public AudioSource soundKalah;

    [Header("Tombol Mute")]
    public Toggle tombolToggle;
    private bool _isMuted = false;

    public static SoundManager instance = null;     //Available tanpa buat new instance

    void Awake()
    {
        instance = this;
    }

    public void bunyiTombol()
    {
        soundTombol.Play();
    }

    //Lagi gak dipake:
    //public void bunyiEfek(AudioClip clip, float volume)
    //{
    //    soundEfek.clip = clip;

    //    soundEfek.volume = volume;

    //    soundEfek.PlayOneShot(soundEfek.clip);
    //}

    public void bunyiKoin()
    {
        soundKoin.volume = .1f;
        soundKoin.Play();
    }

    public void bunyiAngkut()
    {
        soundBel.Play();
        soundAngkut.Play();
    }

    public void bunyiBel()
    {
        soundBel.Play();
    }

    public void bunyiBenturan()
    {
        soundBenturan.Play();
        soundCrash.Play();
    }

    public void bunyiMenang()
    {
        soundMenang.volume = .8f;
        soundMenang.Play();
    }

    public void bunyiMusikMenang()
    {
        soundMenang1.volume = .4f;
        soundMenang1.Play();
    }

    //TODO: 
    public void bunyiKalah()
    {
        soundKalah.volume = .4f;
        soundKalah.Play();
    }

    public void toggleMusik()
    {
        if (tombolToggle.isOn)
        {
            //_isMuted = !_isMuted;
            //AudioListener.volume = 1f;
            musik.UnPause();
        }
        else
        {
            musik.Pause();
            //AudioListener.volume = 0f;
        }
    }
}