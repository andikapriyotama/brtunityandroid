﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class KalkulasiBar : MonoBehaviour
{
    public LevelManager levelManager;
    public float jarakMax;

    private Image img;
    private float time;
    private float lebar;

	void Start ()
    {
        // Jarak Maksimal diatur di LEVEL MANAGER
        jarakMax = levelManager.jarakMax;

        img = GetComponent<Image>();
        img.fillAmount = 0;
	}
	
	void Update ()
    {
        if (!levelManager.selesai)
        {

            if (img.fillAmount < 1)
            {
                // OTW, timer mulai hanya ketika bar masih "nol"
                time += Time.deltaTime;

                img.fillAmount = time / jarakMax;

                if (img.fillAmount > 1)
                {
                    img.fillAmount = 1;

                    levelManager.selesai = true;
                }
            }
            else
            {
                // SUDAH SAMPAI!
                //levelManager.selesai = true;
                //levelManager.jarakHabis = true;
                levelManager.cekGameover();
            }
        }
    }
}