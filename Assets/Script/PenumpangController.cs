﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
using System.Collections.Generic;

public class PenumpangController : MonoBehaviour
{
    public Sprite[] spritePelajar;

    private LevelManager levelManager;
    private GeneratorKendaraan generator;

    void Start ()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        generator = GameObject.Find("GeneratorKendaraan").GetComponent<GeneratorKendaraan>();

        // Ubah sprite secara random
        int i = Random.Range(0, spritePelajar.Length);
        GetComponent<SpriteRenderer>().sprite = spritePelajar[i];
    }

    void OnMouseUp()
    {
        if (!levelManager.selesai)
        {
            if (levelManager.modeAngkut == true)
            {
                if (gameObject.tag == "pelajar" || gameObject.tag == "umum")
                {
                    SoundManager.instance.bunyiAngkut();

                    levelManager.updatePelajar();

                    //Destroy kendaraan yang tampil di scene
                    if (generator.listKendaraan.Count > 0)
                    {
                        PartikelManager.instance.menghilang(
                            new Vector3(
                                generator.listKendaraan[0].transform.position.x + 1.4f,
                                generator.listKendaraan[0].transform.position.y,
                                generator.listKendaraan[0].transform.position.z
                                ));

                        Destroy(generator.listKendaraan[0]);

                        generator.listKendaraan.RemoveAt(0);
                    }
                }

                /*if (gameObject.tag == "umum")
                {
                    SoundManager.instance.bunyiAngkut();

                    levelManager.updateUmum();
                }*/

                if (levelManager.jumlahPelajar == levelManager.pelajarMax)
                    levelManager.cekGameover();

                //Destroy game objek penumpang ini, setelah delay
                Destroy(gameObject, 0.1f);
            }
        }
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}