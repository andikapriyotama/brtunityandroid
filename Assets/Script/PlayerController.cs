﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public LevelManager levelManager;

    public float batasAtasDiScreen = 300f; // Dalam pixel

    private GeneratorKendaraan generator;
    private Rigidbody2D rbBus;
    private Vector3 target;
    private Vector2 sisaJarak;

	void Start()
    {
        //levelManager = GameObject.Find("Skrip").GetComponent<LevelManager>();
        generator = GameObject.Find("GeneratorKendaraan").GetComponent<GeneratorKendaraan>();
        rbBus = GetComponent<Rigidbody2D>();

        SoundManager.instance.bunyiBel();
	}

    void Update()
    {
        //Pindah-pindah ORDER IN LAYER selama bergerak
        if (transform.position.y > -1 * 5.0f / 4.0f)
            GetComponent<SpriteRenderer>().sortingOrder = 0;
        else if (transform.position.y > -1 * 5.0f * 2.0f / 4.0f)
            GetComponent<SpriteRenderer>().sortingOrder = 2;
        else if (transform.position.y > -1 * 5.0f * 3.0f / 4.0f)
            GetComponent<SpriteRenderer>().sortingOrder = 4;
        else if (transform.position.y > -5)
            GetComponent<SpriteRenderer>().sortingOrder = 6;

        // Terima input
        // dari klik atau tap
        if (!levelManager.selesai)
        {
            // Input touch/tap/klik mouse
            if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
                    || (Input.GetMouseButtonDown(0)))
            {
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mousePos.z = 0;

                Vector3 arahTarget = (mousePos - transform.position).normalized;
                if (mousePos.x > transform.position.x)
                {
                    //target maju
                    target = transform.position + (arahTarget * 4.5f);
                }
                else if (mousePos.x < transform.position.x)
                {
                    //target mundur
                    target = transform.position + (arahTarget * 4f);
                }
            }
        }

        sisaJarak = target - transform.position;

        // Membatasi pergerakan,
        // di dalam area gerak saja
        float batasAtas = 0f;
        float batasBawah = -4.5f;
        float batasKiri = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + 1;
        float batasKanan = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - 1;

        transform.position = new Vector2(
            Mathf.Clamp(transform.position.x, batasKiri, batasKanan),
            Mathf.Clamp(transform.position.y, batasBawah, batasAtas)
        );
    }
	
	void FixedUpdate()
    {
        rbBus.velocity = sisaJarak;
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.tag == "kendaraan")
        {
            PartikelManager.instance.ledakan(new Vector3(transform.position.x + 1.4f, transform.position.y, transform.position.z));

            generator.listKendaraan.RemoveAt(generator.listKendaraan.IndexOf(other.gameObject));

            Destroy(other.gameObject);

            Destroy(gameObject);

            if (!levelManager.selesai)
                SoundManager.instance.bunyiBenturan();

            //levelManager.selesai = true;

            //Invoke("keluar", .1f);

            levelManager.cekGameover();
        }
    }
    void keluar()
    {
        levelManager.cekGameover();
    }
}