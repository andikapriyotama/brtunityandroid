﻿using UnityEngine;
using System.Collections;

public class BackupRandomScript : MonoBehaviour
{
    public GameObject kendaraan;

   // private Vector3 layar;
   // private Sprite sp;
   // private Vector2 posisi;

    // Ketika prefab yang dibuat cuma satu
    // Kalau bikin banyak prefab, nanti pakai array GameObject
    // dan di-Instantiate() di Update(),
    // supaya bisa dibuat secara berkala tiap interval tertentu
    void Start ()
    {
        // Posisi clone ditentukan dari
        // (1) posisi titik di ujung kanan layar
        // (2) ditambah jarak dari titik luar ke titik tengah sprite

        // (1) Menentukan titik di ujung kanan layar
        //Vector3 layar = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        //layar = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

        // (2) Menentukan ukuran ujung luar sprite
        //Sprite sp = kendaraan.GetComponent<SpriteRenderer>().sprite;
        //sp = kendaraan.GetComponent<SpriteRenderer>().sprite;

        // Menentukan posisi (x,y) clone
        //Vector2 posisi = new Vector2(layar.x + ((sp.bounds.size.x) / 2), 0);
       // posisi = new Vector2(layar.x + ((sp.bounds.size.x) / 2), 0);

        //Instantiate(kendaraan, posisi, transform.rotation);
        
        InvokeRepeating("munculkanKendaraan", 2, 1);
    }

    // Dipakai ketika mau bikin enemy banyak
    void Update ()
    {
        //InvokeRepeating("munculkanKendaraan",2,1);
    }

    void munculkanKendaraan()
    {
        // x:
        Vector3 layar = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        Sprite sp = kendaraan.GetComponent<SpriteRenderer>().sprite;
        // y:
        Vector3 layarscreen = Camera.main.ScreenToWorldPoint(new Vector3(0, Random.Range(0,300), 0));
        Debug.Log(layarscreen);
        Vector2 posisi = new Vector2(layar.x + ((sp.bounds.size.x) / 2), layarscreen.y);
        //Debug.Log(posisi);

        Instantiate(kendaraan, posisi, transform.rotation);
    }
}