﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AkhirJumlahPenumpang : MonoBehaviour
{
    private Text uiText;

    LevelManager levelManager;
    
    void Start ()
    {
        levelManager = GameObject.Find("Skrip").GetComponent<LevelManager>();

        uiText = GetComponent<Text>();
        uiText.text = levelManager.angkutPenumpang.ToString();
    }
}
