﻿using UnityEngine;
using System.Collections;

public class PartikelManager : MonoBehaviour
{
    public static PartikelManager instance;

    public ParticleSystem efekAsap;
    public ParticleSystem efekApi;

	void Awake ()
    {
        instance = this;
	}
	
	// Update is called once per frame
	public void ledakan(Vector3 posisi)
    {
        bikin(efekApi, posisi);

        bikin(efekAsap, posisi);
	}
    public void menghilang(Vector3 posisi)
    {
        bikin(efekAsap, posisi);
    }

    private ParticleSystem bikin(ParticleSystem prefab, Vector3 posisi)
    {
        ParticleSystem particleSystemBaru = Instantiate(
          prefab,
          posisi,
          Quaternion.identity
        ) as ParticleSystem;

        Destroy(
          particleSystemBaru.gameObject,
          particleSystemBaru.startLifetime
        );

        return particleSystemBaru;
    }
}
